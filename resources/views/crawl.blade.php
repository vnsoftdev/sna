<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">


                @if ($errors->any())
                    <ul>{!! implode('', $errors->all('<li style="color:red">:message</li>')) !!}</ul>
                @endif

                <div class="title">PHP Crawl Demo</div>
                 <form action="{{ action('Crawl@get') }}" method="POST">
                  <input name="url" value="http://sohoa.vnexpress.net/tin-tuc/cong-dong" style="width:300px;" placeholder="A URL please...." /><br/>
                  <input type="submit" name="submit" value="Crawl" class="crawl-btn"/>
                  <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                 </form>
            </div>
        </div>
    </body>
</html>
