<?php

namespace App\Http\Controllers;

use App\Sites;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Libs\SCrawler;
use Illuminate\Support\Facades\Redirect;

class Crawl extends Controller
{
    //
    public function index()
    {
        return view('crawl');
    }

    public function get(Request $request)
    {

        $result = false;
        if ($request->isMethod('post')
            && ($url = $request->input('url'))
            && $this->_isDomainAvailible($url)
        ) {

            $siteData = [
                'url' => $url,
                'title' => ''
            ];

            // search exist site
            $site = Sites::where('url', 'LIKE', $siteData['url'])->get()->first();

            if ($site) {
                $site->update($siteData);
            } else {
                Sites::create($siteData);
            }

            $this->_phpCrawler($url);

            $result = true;
        }

        return redirect()->back()->with($result ? 'message' : 'error', $result ? 'Crawl successful!' : 'Crawl occur some errors');
    }

    protected function _phpCrawler($url)
    {

        // 5 minutes
        set_time_limit(300);

        $crawler = new SCrawler();

        // URL to crawl
        $crawler->setURL($url);

        // Only receive content of files with content-type "text/html"
        $crawler->addContentTypeReceiveRule("#text/html#");

        // Ignore links to pictures, dont even request pictures
        $crawler->addURLFilterRule("#\.(jpg|jpeg|gif|png|pdf|svg|css|js)$# i");

        // Store and send cookie-data like a browser does
        $crawler->enableCookieHandling(true);

        // Set the traffic-limit to 1 MB (in bytes,
        // for testing we dont want to "suck" the whole site)
        //$crawler->setTrafficLimit(6000 * 1024);

        // Should we follow robots.txt
        $crawler->obeyRobotsTxt(true);

//        $crawler->setRequestLimit(1000); // def: 0
//        $crawler->setRequestDelay(4); // second
        $crawler->setCrawlingDepthLimit(3);
//        $crawler->setFollowMode(1); // def: mode 2
//        $crawler->setStreamTimeout(15); // def: 2 seconds

        // Thats enough, now here we go
        $crawler->go();

        // Start crawling by using multi processes
        //$crawler->goMultiProcessed(5);


        // At the end, after the process is finished, we print a short
        // report (see method getProcessReport() for more information)
//            $report = $crawler->getProcessReport();

        // Links followed
//            $report->links_followed;
        // Documents received
//            $report->files_received;
        // Bytes received
//            $report->bytes_received;
        // Process runtime
//            $report->process_runtime;

    }

    protected function _isDomainAvailible($domain)
    {
        if (!filter_var($domain, FILTER_VALIDATE_URL)) {
            return false;
        }
        $curlInit = curl_init($domain);
        curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($curlInit, CURLOPT_HEADER, true);
        curl_setopt($curlInit, CURLOPT_NOBODY, true);
        curl_setopt($curlInit, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curlInit);
        curl_close($curlInit);
        if ($response) {
            return true;
        }
        return false;
    }

}
