<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
//use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class SiteContent extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'site_id', 'url', 'title', 'description', 'bytes_received', 'fulltxt', 'md5sum', 'link_level',
        'content', 'created_date', 'author', 'author_email'
    ];

}
