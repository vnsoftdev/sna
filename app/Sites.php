<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;
//use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Sites extends Eloquent
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'title', 'short_desc',
    ];
    
}
