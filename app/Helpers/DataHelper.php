<?php namespace \App\Helpers;

class DataHelper
{

  public function crawlURL($url)
  {
    $C = new SCrawler();
    $C->setURL($url);
    $C->addContentTypeReceiveRule("#text/html#");/* Only receive HTML pages */
    $C->addURLFilterRule("#(jpg|gif|png|pdf|jpeg|svg|css|js)$# i"); /* We don't want to crawl non HTML pages */
    $C->setTrafficLimit(2000 * 1024);
    $C->obeyRobotsTxt(true); /* Should We follow robots.txt */
    $C->go();
  }

}
