<?php namespace App\Libs;

use App\SiteContent;
use Mockery\CountValidator\Exception;
use Symfony\Component\CssSelector\CssSelectorConverter;

class SCrawler extends \PHPCrawler
{
    /**
     * @param \PHPCrawlerDocumentInfo $docInfo
     */
    public function handleDocumentInfo(\PHPCrawlerDocumentInfo $docInfo)
    {
        // Just detect linebreak for output ("\n" in CLI-mode, otherwise "<br>").
//        if (PHP_SAPI == "cli") $lb = "\n";
//        else $lb = "<br />";

        // Refering URL
        $referer_url = $docInfo->referer_url;
        // URL
        $url = $docInfo->url;
        $status = $docInfo->http_status_code;
        $source = $docInfo->source;
        //if($docInfo->received) $docInfo->bytes_received;

        if($status == 200 && $source != ''){
            echo $url;
            echo '<br>';
//            echo '<pre>'.print_r($docInfo->meta_attributes, true).'</pre>';
//            echo '<pre>'.print_r($this->getContent($source, $url), true).'</pre>';
//            exit();
            flush();


            if( $pageData = $this->getContent($source, $url)) {
                $meta = $docInfo->meta_attributes;
                $siteContentData = [
                    'url' => $url,
                    'title' => @$pageData['title'] ?: @$meta['keywords'],
                    'description' => @$pageData['short_desc'] ?: @$meta['description'],
                    'bytes_received' => $docInfo->bytes_received,
                    'link_level' => $docInfo->url_link_depth,
                    'md5sum' => md5($url),
                    'fulltxt' => $pageData['dom'],
                    'content' => @$pageData['content'] ?: @$pageData['article_content'],
                    'created_date' => @$pageData['created_date'],
                    'author' => @$pageData['author'] ?: @$pageData['author2'],
                    'author_email' => @$pageData['author2_email']
                ];

                $siteContent = SiteContent::where('md5sum', '=', $siteContentData['md5sum'])->get()->first();

                if ($siteContent) {
                    $siteContent->update($siteContentData);
                } else {
                    SiteContent::create($siteContentData);
                }
            }
        }

    }

    /**
     * @param $source
     * @param $url
     * @return array
     */
    public function getContent($source, $url){

        $dom = new \DOMDocument();

        // remove unwanted comments
        $source = $this->removeComments($source);

        // set error level
        $internalErrors = libxml_use_internal_errors(true);

        // load HTML
        $dom->loadHTML($source);

        // Restore error level
        libxml_use_internal_errors($internalErrors);

        // Create XPath object
        $xpath = new \DOMXPath($dom);

        // Remove tags
        $this->removeTags($xpath, ['script', 'link', 'noscript', 'img', 'button', 'input']);

        // Remove style property
        $this->removeAttributes($xpath, ['style', 'href', 'onclick']);

        // Remove empty tags
        $this->removeEmptyTags($xpath);

        //$dom->formatOutput = true;

        // Check domain
        $rs = [];

        $urlPart = \PHPCrawlerUrlPartsDescriptor::fromURL($url);
        switch ($urlPart->domain){
            case 'vnexpress.net':
                $rs = $this->getVnExpress($xpath);
                break;

        }

        try{
            $rs['dom'] = $dom->saveHTML();
        } catch (Exception $e){
            $rs['dom'] = '';
            echo 'error';
        }

        return $rs;
    }

    /**
     * @param \DOMXPath $xpath
     * @param $attributes
     */
    public function removeAttributes(\DOMXPath $xpath, $attributes){
        foreach($attributes as $att) {
            $nodes = $xpath->query('//*[@'.$att.']');  // Find elements with a style attribute
            foreach ($nodes as $node) {              // Iterate over found elements
                $node->removeAttribute($att);    // Remove style attribute
            }
        }
    }

    /**
     * @param \DOMXPath $xpath
     * @param $tagNames
     */
    public function removeTags(\DOMXPath $xpath, $tagNames){
        if(is_array($tagNames)){
            foreach($tagNames as $tagName){
                $this->removeTags($xpath, $tagName);
            }
        } else {
            $nodeList = $xpath->query('//' . $tagNames);
            foreach($nodeList as $node){
                $node->parentNode->removeChild($node);
            }
        }
    }

    /**
     * @param $source
     * @return mixed
     */
    public function removeComments($source){
        return preg_replace('/<!--(.|\s)*?-->/', '', $source);
    }

    /**
     * @param \DOMXPath $xpath
     */
    public function removeEmptyTags(\DOMXPath $xpath){

        foreach( $xpath->query('//*[not(node())]') as $node ) {
            $node->parentNode->removeChild($node);
        }

    }

    public function getDomData(\DOMXPath $xpath, $query, $Limit = 0){

        //$nodeList = $xpath->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $className ')]");
        return $this->_getNodeListValue($xpath->query($query), 1);
    }



    /**
     *
     * Structure
     *
     * div#detail_page
     *  div.main_content_detail
     *      div.block_timer_share
     *          div.block_timer         - datetime
     *      div.title_news              - title
     *      div.short_intro             - short desc
     *      div#left_calculator
     *          div.fck_detail          - content
     *              p
     *                  strong          - author
     *                  i               - email
     */
    public function getVnExpress(\DOMXPath $xpath){

        $rs = $this->_parsePageContent($xpath, [
            'title'         => 'div.main_content_detail div.title_news',
            'short_desc'    => 'div.main_content_detail div.short_intro',
            'content'       => 'div.main_content_detail div.fck_detail',
            'created_date'  => 'div.main_content_detail div.block_timer_share div.block_timer',
            'author'        => 'div.main_content_detail div.fck_detail p.author',
            'author2'       => 'div.main_content_detail div.fck_detail p > strong',
            'author2_email' => 'div.main_content_detail div.fck_detail p > i',

            'article_content'   => 'div.main_content_detail div#article_content'
        ]);

        return $rs;
    }



    protected function _parsePageContent(\DOMXPath $xpath, array $properties){
        $rs = [];
        $cssSelector = new CssSelectorConverter();
        foreach($properties as $key => $selector){
            $rs[ $key ] = $this->getDomData($xpath, $cssSelector->toXPath($selector), 1);
        }
        return $rs;
    }





    /**
     * Get meta data
     *
     * @deprecated
     *
     * @param \DOMXPath $xpath
     * @param array $names
     * @return array
     */
    public function getMetaData(\DOMXPath $xpath, array $names){

        $arr = [];
        foreach($names as $name){
            $arr[ $name ] = $this->_getNodeListValue($xpath->query('//meta[@name="'.$name.'"]/@content'), 1);
        }

        return $arr;
    }

    /**
     * @param \DOMNodeList $nodeList
     * @param int $limit
     * @return array|null|string
     */
    protected function _getNodeListValue(\DOMNodeList $nodeList, $limit = 0){
        $rs = null;
        if($nodeList->length){
            if($limit == 1){
                return $this->_fmtStr($nodeList->item(0)->nodeValue);
            } else {
                $rs = [];
                for($i = 0; $i < $nodeList->length; $i++){
                    $rs[$i] = $this->_fmtStr($nodeList->item($i)->nodeValue);
                }
            }
        }
        return $rs;
    }

    protected function _fmtStr($str){
        return trim(preg_replace('~\R~u', "\r\n", $str));
    }

}

