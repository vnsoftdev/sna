<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('site_id')->index()->nullable();
            $table->string('url')->nullable();
            $table->string('md5sum', 32)->nullable();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->float('bytes_received')->nullable();
            $table->integer('link_level')->nullable();
            $table->text('fulltxt')->nullable();
            $table->mediumText('content')->nullable();
            $table->string('created_date', 32)->nullable();
            $table->string('author')->nullable();
            $table->string('author_email')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('site_contents');
    }
}
